package com.example.helper.ui.favorites

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import android.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.helper.R
import com.example.helper.adapter.BSFAdapter
import com.example.helper.adapter.FavoritesAdapter
import com.example.helper.adapter.SwipeCallBack
import com.example.helper.api.RetrofitClient
import com.example.helper.model.Car
import com.example.helper.model.Favorites
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_bs_favorites.*
import kotlinx.android.synthetic.main.fragment_favorites.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavoritesFragment : Fragment() {

    private var listFav: ArrayList<Favorites> = ArrayList()

    private var listCar: ArrayList<Car> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_favorites, container, false)

        val abf = root.findViewById<Toolbar>(R.id.abf)

        abf.inflateMenu(R.menu.items_favorites)

        abf.setOnMenuItemClickListener { onOptionsItemSelected(item = it) }

        val btn = abf.findViewById<Button>(R.id.menuBackH)

        btn.setOnClickListener {
            requireActivity().findViewById<DrawerLayout>(R.id.drawer_layout)
                .openDrawer(GravityCompat.START)
        }


        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val lay = view.findViewById<View>(R.id.fbsf)
        val but = lay.findViewById<Button>(R.id.addNewFav)
        but.setOnClickListener {
            populateCar()
        }

        refreshFav.setOnRefreshListener {
            refreshFav.isRefreshing = true
            populateFavorites()
            refreshFav.isRefreshing = false
        }

        rvFavorites.addItemDecoration(
            DividerItemDecoration(
                requireActivity(),
                DividerItemDecoration.VERTICAL
            )
        )
        populateFavorites()

        rvAddNewFav.addItemDecoration(
            DividerItemDecoration(
                requireActivity(),
                DividerItemDecoration.VERTICAL
            )
        )
        populateCar()

        val swipeHandler = object : SwipeCallBack(requireActivity()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = rvFavorites.adapter as FavoritesAdapter

                val position = viewHolder.adapterPosition

                if (direction == ItemTouchHelper.LEFT) {
                    val deletedModel = listFav[position]
                    adapter.removeItem(position)

                    val snackbar = Snackbar.make(
                        requireActivity().window.decorView.rootView,
                        "${deletedModel.name} " + getString(R.string.removeItemMessage),
                        Snackbar.LENGTH_LONG
                    )
                    snackbar.setAction(getString(R.string.undoActionText)) {
                        adapter.restoreItem(deletedModel, position)
                    }
                    snackbar.setActionTextColor(Color.YELLOW)
                    snackbar.show()
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(rvFavorites)

    }

    private fun populateFavorites() {
        val token = requireActivity().intent.getStringExtra("token")

        RetrofitClient.instance.getAllFavorites(token)
            .enqueue(object : Callback<ArrayList<Favorites>> {
                override fun onFailure(call: Call<ArrayList<Favorites>>, t: Throwable) {
                    Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<ArrayList<Favorites>>,
                    response: Response<ArrayList<Favorites>>
                ) {
                    if (response.isSuccessful) {
                        listFav.clear()
                        rvFavorites.adapter = FavoritesAdapter(listFav, token)
                        listFav.addAll(response.body()!!)
                        rvFavorites.layoutManager = LinearLayoutManager(requireActivity())
                        rvFavorites.adapter = FavoritesAdapter(listFav, token)
                    } else {
                        Toast.makeText(requireActivity(), response.message(), Toast.LENGTH_LONG)
                            .show()
                    }
                }

            })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.clearAll -> {
                val builder = AlertDialog.Builder(requireActivity())
                builder.setTitle(getString(R.string.infoAlertFav))
                    .setMessage(getString(R.string.messageAlertFav))
                    .setPositiveButton(getString(R.string.yesMessage)) { id, btn ->
                        deleteAllFavorites()
                    }
                    .setNegativeButton(getString(R.string.cancelMessage)) { id, btn ->

                    }

                val alert = builder.create()
                alert.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun populateCar() {
        val token = requireActivity().intent.getStringExtra("token")

        RetrofitClient.instance.getCars().enqueue(object : Callback<ArrayList<Car>> {
            override fun onFailure(call: Call<ArrayList<Car>>, t: Throwable) {
                Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ArrayList<Car>>,
                response: Response<ArrayList<Car>>
            ) {
                if (response.isSuccessful) {
                    listCar.clear()
                    rvAddNewFav.adapter = BSFAdapter(listCar, token, requireContext())
                    listCar.addAll(response.body()!!)
                    rvAddNewFav.layoutManager = LinearLayoutManager(requireActivity())
                    rvAddNewFav.adapter = BSFAdapter(listCar, token, requireContext())
                } else {
                    Toast.makeText(requireActivity(), response.message(), Toast.LENGTH_LONG)
                        .show()
                }
            }


        })
    }


    private fun deleteAllFavorites() {
        val token = requireActivity().intent.getStringExtra("token")

        RetrofitClient.instance.removeAllFavorites(token).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {}

        })
    }
}