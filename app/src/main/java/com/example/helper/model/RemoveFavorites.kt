package com.example.helper.model

data class RemoveFavorites (
    val _id: String
)